﻿unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, ComObj,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.DBCtrls, Data.Win.ADODB, Vcl.Grids, Vcl.DBGrids, Unit2, GsvThread;

type
  TForm1 = class(TForm)
    DBGrid1: TDBGrid;
    dsObj: TDataSource;
    ADOCON: TADOConnection;
    qruObj: TADOQuery;
    DBNavigator1: TDBNavigator;
    Button1: TButton;
    qruObjid: TAutoIncField;
    qruObjName: TWideStringField;
    qruObjcomment: TWideMemoField;
    Button2: TButton;
    Button3: TButton;
    Memo: TDBMemo;
    qruObjVal: TADOQuery;
    dsObjVal: TDataSource;
    Edit1: TEdit;
    Label1: TLabel;
    Button4: TButton;
    procedure qruObjcommentGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  MyThread: Unit2.MyThread;
implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  i: Integer;
  fStr: string;
  fDt: TDataSet;
  fTf: TextFile;
begin
  fDt := DBGrid1.DataSource.DataSet;
  // fBm := fDt.Bookmark;
  fDt.DisableControls;
  AssignFile(fTf, 'd:\Out.txt');
  Rewrite(fTf);
  try
    fDt.First;
    while not fDt.Eof do
    begin
      fStr := '|';
      for i := 0 to Pred(DBGrid1.Columns.Count) do
      begin
        if not DBGrid1.Columns[i].Visible then // Пропускаем невидимые столбцы
          Continue;
        fStr := fStr + fDt.FieldByName(DBGrid1.Columns[i].FieldName)
          .AsString + '|';
      end;
      Writeln(fTf, fStr);
      fDt.Next
    end;
  finally
    ShowMessage('Export complited!');
    // fDt.Bookmark := fBm;
    fDt.EnableControls;
    CloseFile(fTf);
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  i, j: Integer;
  ExApp, WB, WS: Variant;
begin
  i := 1;
  ExApp := CreateOleObject('Excel.Application');
  WB := ExApp.Workbooks.Add;
  WS := ExApp.Workbooks[1].WorkSheets[1];
  with DBGrid1 do
  begin
    DataSource.DataSet.First;
    for j := 0 to Columns.Count - 1 do
      WS.cells[1, j + 1].Value := DBGrid1.Columns[j].Title.Caption;
    while not DataSource.DataSet.Eof do
    begin
      for j := 0 to (Columns.Count - 1) do
      begin
        WS.cells[i + 1, j + 1].Value := Columns[j].Field.AsString;
      end;
      DataSource.DataSet.Next;
      Inc(i);
    end;
    DataSource.DataSet.EnableControls;
    ExApp.Visible := true;
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
var

  j, i: Integer;
begin
  Memo.Clear;

  if Edit1.Text <> '' then
  begin
    j := strtoint(Edit1.Text);
    for i := 1 to j do
    begin
      Memo.Lines.Add('10');
    end;
  end
  else
  Showmessage('Введите количество повторений!');
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
//Вначале нужно создать экземпляр потока:
  MyThread := Unit2.MyThread.Create(False);
//Параметр False запускает поток сразу после создания, True - запуск впоследствии , методом Resume
//Далее можно указать параметры потока, например приоритет:
  MyThread.Priority:=tpNormal;
  MyThread.Terminate;
end;

procedure TForm1.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
if Not (Key in ['0'..'9', #8])     //
 then Key:=#0;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Memo.Clear;
  qruObj.Open;
  qruObjVal.Open;
end;

procedure TForm1.qruObjcommentGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  Text := Sender.AsString;   // В обычном гриде нет возможности включить отображение мемо полей, поэтому здесь есть такой код, в грид Ехлиба такое свойство есть
end;

end.
