object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 348
  ClientWidth = 854
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 598
    Top = 1
    Width = 123
    Height = 13
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1087#1086#1074#1090#1086#1088#1077#1085#1080#1081
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 8
    Width = 585
    Height = 225
    DataSource = dsObj
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'id'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Name'
        Width = 203
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'comment'
        Width = 132
        Visible = True
      end>
  end
  object DBNavigator1: TDBNavigator
    Left = 0
    Top = 239
    Width = 580
    Height = 25
    DataSource = dsObj
    TabOrder = 1
  end
  object Button1: TButton
    Left = 0
    Top = 270
    Width = 121
    Height = 25
    Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' '#1092#1072#1081#1083' txt'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 0
    Top = 301
    Width = 121
    Height = 25
    Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 725
    Top = 20
    Width = 121
    Height = 25
    Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1084#1077#1084#1086
    TabOrder = 4
    OnClick = Button3Click
  end
  object Memo: TDBMemo
    Left = 661
    Top = 51
    Width = 185
    Height = 271
    DataField = 'obj_id'
    DataSource = dsObjVal
    TabOrder = 5
  end
  object Edit1: TEdit
    Left = 598
    Top = 22
    Width = 121
    Height = 21
    TabOrder = 6
    OnKeyPress = Edit1KeyPress
  end
  object Button4: TButton
    Left = 336
    Top = 297
    Width = 177
    Height = 25
    Caption = 'Button4'
    TabOrder = 7
    OnClick = Button4Click
  end
  object dsObj: TDataSource
    DataSet = qruObj
    Left = 160
    Top = 272
  end
  object ADOCON: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;Initial Catalog=SoftPoint;Data Source=WIN-VSBGCEPLL7E\S' +
      'QLEXPRESS'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 208
    Top = 272
  end
  object qruObj: TADOQuery
    Active = True
    Connection = ADOCON
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select id, Name, comment from test_Object')
    Left = 256
    Top = 272
    object qruObjid: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object qruObjName: TWideStringField
      FieldName = 'Name'
      Size = 100
    end
    object qruObjcomment: TWideMemoField
      FieldName = 'comment'
      OnGetText = qruObjcommentGetText
      BlobType = ftWideMemo
    end
  end
  object qruObjVal: TADOQuery
    Active = True
    Connection = ADOCON
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from test_ObjectValue')
    Left = 592
    Top = 48
  end
  object dsObjVal: TDataSource
    DataSet = qruObjVal
    Left = 624
    Top = 48
  end
end
