object Form3: TForm3
  Left = 0
  Top = 0
  Hint = #1054#1090#1082#1088#1099#1090#1100' '#1092#1072#1081#1083
  Caption = 'Form3'
  ClientHeight = 333
  ClientWidth = 762
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 762
    Height = 333
    Align = alClient
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'id'
        Width = -1
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'Name'
        Title.Caption = #1053#1086#1084#1077#1088
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Info'
        Title.Caption = #1047#1085#1072#1095#1077#1085#1080#1077
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Time'
        Title.Caption = #1044#1072#1090#1072
        Width = 100
        Visible = True
      end>
  end
  object ActList: TActionList
    Left = 56
    Top = 120
  end
  object MainMenu1: TMainMenu
    Left = 112
    Top = 120
    object N1: TMenuItem
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1092#1072#1081#1083
      Hint = #1042#1099#1073#1077#1088#1080#1090#1077' '#1092#1072#1081#1083
      OnClick = N1Click
    end
    object txt1: TMenuItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' txt'
      OnClick = txt1Click
    end
    object Excel1: TMenuItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      OnClick = Excel1Click
    end
    object N2: TMenuItem
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1087#1088#1086#1075#1088#1072#1084#1084#1091
      OnClick = N2Click
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 176
    Top = 120
  end
  object ACon: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;Initial Catalog=MGTS;Data Source=WIN-VSBGCEPLL7E\SQLEXP' +
      'RESS'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 432
    Top = 136
  end
  object qru: TADOQuery
    Active = True
    Connection = ACon
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select  p.Name, s.Spec_Name from Personal p '
      ' inner join Personal_Spec_Info as ps on p.id = ps.Name_Id'
      ' inner join Speciality as s on s.id = ps.Spec_Id')
    Left = 472
    Top = 136
  end
  object ds: TDataSource
    DataSet = qru
    Left = 520
    Top = 136
  end
  object DataSource1: TDataSource
    DataSet = ATable
    Left = 264
    Top = 192
  end
  object ADOQuery1: TADOQuery
    Active = True
    Connection = ACon
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from TextTable'
      '')
    Left = 264
    Top = 136
  end
  object ATable: TADOTable
    Active = True
    Connection = ACon
    CursorType = ctStatic
    TableName = 'TextTable'
    Left = 368
    Top = 144
  end
end
