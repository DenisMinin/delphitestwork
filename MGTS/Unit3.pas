unit Unit3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, ComObj,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.TitleBarCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.ToolWin, Vcl.ComCtrls, Vcl.ActnMan, Vcl.ActnCtrls,
  System.Actions, Vcl.ActnList, Vcl.Menus, Data.Win.ADODB;

type
  TForm3 = class(TForm)
    DBGrid1: TDBGrid;
    ActList: TActionList;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    OpenDialog1: TOpenDialog;
    ACon: TADOConnection;
    qru: TADOQuery;
    ds: TDataSource;
    DataSource1: TDataSource;
    ADOQuery1: TADOQuery;
    ATable: TADOTable;
    txt1: TMenuItem;
    Excel1: TMenuItem;
    procedure N2Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure txt1Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm3.Excel1Click(Sender: TObject);
var
  i, j: Integer;
  ExApp, WB, WS: Variant;
begin
  i := 1;
  ExApp := CreateOleObject('Excel.Application');
  WB := ExApp.Workbooks.Add;
  WS := ExApp.Workbooks[1].WorkSheets[1];
  with DBGrid1 do
  begin
    DataSource.DataSet.First;
    for j := 0 to Columns.Count - 1 do
      WS.cells[1, j + 1].Value := DBGrid1.Columns[j].Title.Caption;
    while not DataSource.DataSet.Eof do
    begin
      for j := 0 to (Columns.Count - 1) do
      begin
        WS.cells[i + 1, j + 1].Value := Columns[j].Field.AsString;
      end;
      DataSource.DataSet.Next;
      Inc(i);
    end;
    DataSource.DataSet.EnableControls;
    ExApp.Visible := true;
  end;
end;

procedure TForm3.N1Click(Sender: TObject);
var
  F: textfile;
  s: string;
begin

  with OpenDialog1, DBGrid1 do
    if Execute then
      AssignFile(F, FileName);
  Reset(F);

  While not Eof(F) do
  begin
    ReadLn(F, s);

    if pos('|', s) <> 0 then

    begin

      ATable.Insert;

{      ATable.FieldByName('Id').AsString :=copy(s,1,pos('|',s)-1); }
      delete(s,1,pos('|',s));

      ATable.FieldByName('Name').AsString :=copy(s,1,pos('|',s) - 1);
              delete(s,1,pos('|',s));

      ATable.FieldByName('Info').AsString :=copy(s,1,pos('|',s)-1);
        delete(s,1,pos('|',s));

      ATable.FieldByName('Time').AsString := DateToStr(Now);

      ATable.Post;
    end

    else if s = '' then
    begin

      DataSource1.DataSet.Insert;

      DataSource1.DataSet.FieldByName('NAme').AsString := '';

      DataSource1.DataSet.Post;

    end;
  end;
end;

procedure TForm3.N2Click(Sender: TObject);
begin
  Close;
end;

procedure TForm3.txt1Click(Sender: TObject);
var
  i: Integer;
  fStr: string;
  fDt: TDataSet;
  fTf: TextFile;
begin
  fDt := DBGrid1.DataSource.DataSet;
  // fBm := fDt.Bookmark;
  fDt.DisableControls;
  AssignFile(fTf, 'd:\DBOut.txt');
  Rewrite(fTf);
  try
    fDt.First;
    while not fDt.Eof do
    begin
      fStr := '|';
      for i := 0 to Pred(DBGrid1.Columns.Count) do
      begin
        if not DBGrid1.Columns[i].Visible then // ���������� ��������� �������
          Continue;
        fStr := fStr + fDt.FieldByName(DBGrid1.Columns[i].FieldName)
          .AsString + '|';
      end;
      Writeln(fTf, fStr);
      fDt.Next
    end;
  finally
    ShowMessage('Export complited!');
    // fDt.Bookmark := fBm;
    fDt.EnableControls;
    CloseFile(fTf);
  end;
end;



end.
